import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions


class Base:

    def do_send_keys(self, by_locator, text):
        WebDriverWait(self, 10).until(expected_conditions.visibility_of_element_located(by_locator)).send_keys(text)

    def do_click(self, by_locator):
        WebDriverWait(self, 10).until(expected_conditions.visibility_of_element_located(by_locator)).click()

    def is_visible(self, by_locator):
        element = WebDriverWait(self, 10).until(expected_conditions.visibility_of_element_located(by_locator))
        return bool(element)

    def scroll_down(self):
        scrll_dwn = self.find_element(By.TAG_NAME, "html")
        scrll_dwn.send_keys(Keys.END)
        time.sleep(1)