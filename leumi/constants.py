from selenium.webdriver.common.by import By

class Constants:

    BASE_URL = "https://www.btl.gov.il/Pages/default.aspx"
    CACL_URL = "https://www.btl.gov.il/Simulators/Pages/NifgaeyAvodaCalc.aspx"
    HOME_PAGE_TITLE = "דף הבית, הביטוח הלאומי"
    HURT_DATE = "25/01/1999"
    BRTH_DATE = "18/07/1990"
    WRONG_HURT_DATE = "40/01/3000"
    WRONG_BRTH_DATE = "20/07/4000"
    FROM_DATE = "25/01/2000"
    TO_DATE = "25/01/2020"
    SALARY = "5000"

    # Locators
    HURT_DATE_LOC = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_DynDatePicker_PguiaDate_Date")
    BRTH_DATE_LOC = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_DynDatePicker_BirthDate_Date")
    CONTINUE_BTN = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_StartNavigationTemplateContainerID_StartNextButton")
    CALC_BTN = (By.ID, "ctl00_PlaceHolderMain_ToolBarControl_Repeater1_ctl01_a")
    HRT_BTN = (By.LINK_TEXT, "נפגעי עבודה")
    WORK_ACDNT_BTN = (By.LINK_TEXT, "נפגעי עבודה")
    ESTIMATE_BTN = (By.LINK_TEXT, "חישוב גמלת נכות מעבודה")
    PERCENT_DISABILITY = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_Percent")
    FROM_DATE_LOC = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_DynDatePicker_From_Date")
    TO_DATE_LOC = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_DynDatePicker_To_Date")
    SAVE_BTN = (By.LINK_TEXT, "שמירת ליקוי")
    CONTINUE_AFTER_SAVE_BTN = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_StepNavigationTemplateContainerID_StepNextButton")
    RADIO_BTN = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_rdb_IncomeTypeCalculation_0")
    SALARY_INPUT = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_txt_totalBassis")
    LAST_CONTINUE_BTN = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_StepNavigationTemplateContainerID_StepNextButton")
    ALERT = (By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_DynDatePicker_PguiaDate_error")
