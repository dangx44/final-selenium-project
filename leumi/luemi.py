import time
from leumi.constants import Constants
from BaseFunctions.baseFunctions import Base
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import os

class Luemi(webdriver.Chrome, Constants, Base):

    def __init__(self, driver_path=r"/usr/local/bin", teardown=False):
        self.driver_path = driver_path
        self.teardown = teardown
        os.environ['PATH'] += self.driver_path
        super(Luemi, self).__init__()
        self.implicitly_wait(15)
        self.maximize_window()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.teardown:
            self.quit()

    def land_first_page(self):
        self.get(Constants.BASE_URL)

    def land_calc_first_page(self):
        self.get(Constants.CACL_URL)

    def get_home_page_title(self):
        self.land_first_page()
        return self.title

    def start_calculation_flow(self):
        self.do_click(self.CALC_BTN)
        self.scroll_down()
        self.do_click(self.HRT_BTN)
        self.do_click(self.ESTIMATE_BTN)

    def drop_down_percent(self):
        self.do_send_keys(self.PERCENT_DISABILITY, "50")
        drop_down_element = self.find_element(By.ID, "ctl00_ctl49_g_0339dd53_9d84_4774_b0c6_a578bf46ed47_ctl00_WorkDisabilityWizard_ddl_Type")
        drp = Select(drop_down_element)
        drp.select_by_value("1")

    def fill_first_dates(self, hurt_date, brth_date):
        self.do_send_keys(self.HURT_DATE_LOC, hurt_date)
        self.do_send_keys(self.BRTH_DATE_LOC, brth_date)
        self.scroll_down()
        self.do_click(self.CONTINUE_BTN)

    def fill_second_dates(self, from_date, to_date):
        self.do_send_keys(self.FROM_DATE_LOC, from_date)
        self.do_send_keys(self.TO_DATE_LOC, to_date)
        self.scroll_down()
        time.sleep(1)
        self.do_click(self.SAVE_BTN)
        self.do_click(self.CONTINUE_AFTER_SAVE_BTN)

    def last_page(self):
        self.do_click(self.RADIO_BTN)
        self.do_send_keys(self.SALARY_INPUT, Constants.SALARY)
        self.execute_script("window.scrollBy(0,500)")
        self.do_click(self.LAST_CONTINUE_BTN)

    def does_alert_msg_exist(self):
        return self.is_visible(self.CONTINUE_BTN)

    def does_percent_input_exist(self):
        return self.is_visible(self.PERCENT_DISABILITY)