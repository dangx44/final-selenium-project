# Final Selenium Project

## Description:
This repository runs 2 programs.  One as a complete flow starting from the Bituach Leumi Homepage and complletes an entire disability calculation.  And the second program runs 3 specific tests.

1.Ui Test 
2. Negative Case Test
3. Positive Case Test



## How to run the flow file:

Have to install selenium, pytest & Chrome Webdriver.  Change the driver_path in luemi.py to where you stored Chrome Webdriver. 

1. Left click on run_full_calc_flow.py and select 'run'.  This will run the entire flow starting from the Homepage all the way to the end of the disability calculation.  It gives the same output consistently.


## How to run the test file:

1. Go to terminal and run the py.test command.
2. If it doesn't work, might have to cd into the Tests directory and run from there.
3. Can select a specific test out of the 3 tests with the -k flag
