import time
from leumi.constants import Constants
from leumi.luemi import Luemi
import pytest

#Ui Test Case
def test_m1():
    with Luemi() as bot:
        bot.land_first_page()
        bot.get_home_page_title()
        assert bot.title == Constants.HOME_PAGE_TITLE

#Negative Test Case
def test_m2():
    with Luemi() as bot:
        bot.land_calc_first_page()
        bot.fill_first_dates(Constants.WRONG_HURT_DATE, Constants.WRONG_BRTH_DATE)
        time.sleep(3)
        assert bot.does_alert_msg_exist() == True

# Positive Test Case
def test_m3():
    with Luemi() as bot:
        bot.land_calc_first_page()
        bot.fill_first_dates(Constants.HURT_DATE, Constants.BRTH_DATE)
        assert bot.does_percent_input_exist() == True
