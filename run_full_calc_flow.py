from leumi.constants import Constants
from leumi.luemi import Luemi

with Luemi() as bot:
    bot.land_first_page()
    bot.start_calculation_flow()
    bot.fill_first_dates(Constants.HURT_DATE, Constants.BRTH_DATE)
    bot.drop_down_percent()
    bot.fill_second_dates(Constants.FROM_DATE, Constants.TO_DATE)
    bot.last_page()